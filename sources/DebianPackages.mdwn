There are these packages in _unstable_ which you probably want to install: 

* [[xserver-xorg-video-nouveau|http://packages.debian.org/unstable/xserver-xorg-video-nouveau]] The 2D DDX 
* [[libgl1-mesa-dri|http://packages.debian.org/unstable/libgl1-mesa-dri]] The 3D drivers

Other packages will automatically be installed as dependencies, such as 

* [[libdrm2|http://packages.debian.org/unstable/libdrm2]] 
* [[libdrm-nouveau2|http://packages.debian.org/unstable/libdrm-nouveau2]] 

Newer versions of these packages are occasionally available in Debian's [[experimental|http://wiki.debian.org/DebianExperimental]] repository. 

If you run into problems, you may want to install various -dbg packages to debug them: 

* [[xserver-xorg-video-nouveau-dbg|http://packages.debian.org/unstable/xserver-xorg-video-nouveau-dbg]] 
* [[libgl1-mesa-dri-dbg|http://packages.debian.org/unstable/libgl1-mesa-dri-dbg]] 
* [[libgl1-mesa-glx-dbg|http://packages.debian.org/unstable/libgl1-mesa-glx-dbg]] 
* [[libdrm-nouveau2-dbg|http://packages.debian.org/unstable/libdrm-nouveau2-dbg]] 
* [[libdrm2-dbg|http://packages.debian.org/unstable/libdrm2-dbg]] 
* [[xserver-xorg-core-dbg|http://packages.debian.org/unstable/xserver-xorg-core-dbg]] 
