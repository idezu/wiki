English / [[Español|Portada_Español]] / [[Français|FrontPage-fr]] / [[Português|FrontPage-pt-br]] / [[Русский|Заглавная_страница]] / [[Italiano|FrontPage-it]] / [[Türkçe|Anasayfa]] 

[[Bugs]]  [[Source]]  [[Development]] [[Translation_Team|Translation_Team]] 


# Nouveau: Accelerated Open Source driver for nVidia cards


### About Nouveau

The **nouveau** project aims to build high-quality, free/libre software drivers for [[nVidia cards|CodeNames]].  “Nouveau” [_nuvo_] is the French word for “new”. 

If you have questions or problems, please have a look at pages **[[TroubleShooting]]**, **[[Bugs]]** and **[[FAQ]]** before contacting us. 

Most development talk occurs in the [[#nouveau|irc://irc.freenode.net/nouveau]] **IRC channel** on [[freenode|http://freenode.net/]].  Raw logs and edited summaries (the so-called _TiNDC_s, or The irregular Nouveau Development Companions) can be found on the **[[IRC chat logs|IrcChatLogs]]** page. 

We also use several **mailing lists**: bugzilla reports, patches, discussion and announcements are sent to the **[[nouveau|http://lists.freedesktop.org/mailman/listinfo/nouveau]]** list, and changes to the Nouveau DDX and some development tools (e.g., _rules-ng_ and _renouveau_) and hardware documentation are reported to the [[CVS commits|http://lists.sourceforge.net/lists/listinfo/nouveau-cvs]] list.  The [[dri-devel|http://lists.freedesktop.org/mailman/listinfo/dri-devel]] and [[mesa-dev|http://lists.freedesktop.org/mailman/listinfo/mesa-dev]] lists are also important; see [[DRI mailing lists|http://dri.freedesktop.org/wiki/MailingLists]] for their descriptions, and please notice the difference between end-user support lists and developer lists. 

We are also interested in what is said about the project, so links are collected in the page [[InThePress]]. 

The Nouveau driver suite consists of three major parts: the kernel modules (DRM), the X driver (DDX), and [[the 3D driver|MesaDrivers]] (in Mesa, optional). In addition, libdrm with Nouveau support is required. You can find all these in [[Source]] page. 

<a name="Status"></a> 
### Current Status

Quick overview: 

* [[KernelModeSetting]] (KMS) is mandatory 
* Suspend and resume is working for many chips 
* 2D support is in very good shape with EXA acceleration, Xv and [[Randr12]] (dual-head, rotations, etc.). To understand how multiple monitors work in X, see [[MultiMonitorDesktop]]. 
* For some Fermi cards, like the [[NVD9|CodeNames]], check the [[firmware status|InstallDRM]] as it is required for acceleration. 
* Some 3D acceleration exists. Read [[MesaDrivers]] carefully. 
* [[Nvidia Optimus|Optimus]] support is not implemented yet, you may not be able to use the Nvidia GPU at all on Optimus laptops. 
See the [[status matrix|FeatureMatrix]] for functionality supported on each chipset. 

Each card specific status [[here|HardwareStatus]] (very incomplete) 

Users can help the development by installing Nouveau and trying to use it for regular desktop activities including web surfing, watching videos, 2D games (not using OpenGL). Use dual-head, try tv-out. Any encountered problems should be found in the bugzilla or in this wiki. If not, [[submit a report|Bugs]], please. Accelerated OpenGL, although progressing, is not yet supported. 

Also, specific testing requests may be found in the [[TestersWanted]] page. 

If you have nVidia hardware you don't need, [[donating|HardwareDonations]] it could be helpful! 


#### Installing

For developers and advanced users, we have instructions on **[[compiling and using nouveau|InstallNouveau]]**. 


### Getting Involved

You want to involve in Nouveau development? We have simple tasks for to begin with on [[the pathscale wiki|http://github.com/pathscale/pscnv/wiki/TODO-for-newcomers]]. 

As you can see, there is a lot of work for people not willing to write code! Please show-up on IRC and ask the possible mentor how to get started. 


### News

* _**17.12.2012 posted by mupuf**_ : Thermal + fan management has been merged to Nouveau's DRM tree. Documentation is [[here|http://cgit.freedesktop.org/nouveau/linux-2.6/tree/Documentation/thermal/nouveau_thermal]] and my call for testing is [[here|http://www.spinics.net/lists/dri-devel/msg31902.html]]. 

* _**18.6.2012 posted by mupuf**_ : Three months ago, Nouveau was out of staging. Following this, libdrm was rewritten and released as "stable". Mesa drivers then were rebased on top of it and are being stabilized for Mesa 8.1. The last missing piece to be released as stable was xf86-video-nouveau, the nouveau DDX. Consider it done [[since yesterday|http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/commit/?id=6dbf4ea12600275775123f9f564469454415da55]], Ben Skeggs [[released the version 1.0|http://nouveau.freedesktop.org/release/]] of the DDX. 

* _**30.3.2012 posted by calim**_ : Initial acceleration support for Kepler has been [[committed|http://cgit.freedesktop.org/nouveau/linux-2.6/log/]]. [[External firmware|NVC0_Firmware]] is still required for now. The DDX also requires the [[new libdrm|https://github.com/skeggsb/]], which will be merged soon. 

* _**29.3.2012 posted by shinpei**_ : An open-source CUDA driver and runtime (but not compiler) is now available with [[Gdev|https://github.com/shinpei0208/gdev]]. 

* _**22.3.2012 posted by shinpei**_ : Nouveau is leaving staging! That is, Nouveau will be part of the mainline Linux kernel. 

* _**17.1.2012 posted by lynxeye**_ : We are at [[FOSDEM 2012|http://fosdem.org/2012/]]. There will be an presentation about nouveau and a few devs can be found in the X.Org [[DevRoom]] to answer your questions. Here are the [[slides|https://github.com/mupuf/xdc2011-nouveau/blob/fosdem2012/nouveau.pdf]]. 

This wiki is undergoing [[conversion]]. If you have a fd.o shell account, you can help!
