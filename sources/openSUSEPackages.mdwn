# Nouveau drivers for openSUSE

This page will guide you with the installation of Nouveau drivers for openSUSE. 

## Installation

On recent versions of openSUSE the nouveau driver packages will be installed automatically. 

The X11:XOrg repository (see [[http://download.opensuse.org/repositories/X11:/XOrg/]]) may provide more up-to-date packages but they are mostly already outdated when they appear. 

The former home:jobermayr with daily packages was dropped in early openSUSE 12.2 time: [[http://bugzilla.novell.com/show_bug.cgi?id=771392#c76]] 

### Troubleshooting

See [[http://en.opensuse.org/SDB:Configuring_graphics_cards]] for OpenSUSE-specific troubleshooting options. 
