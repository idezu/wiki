[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[TiNDC 2008|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_33]]/[[ES|Nouveau_Companion_33-es]]/[[FR|Nouveau_Companion_33-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Issue for January, 12th


### Intro

A Happy New Year to all of our readers. Welcome back to the first issue of the TiNDC in the new year. 

Let me take the opportunity to thank all our contributors and testers for their work  during 2007. Hopefully we will see at least some of you at FOSDEM this year. 

The last issue was a bit hushed due to the holidays and me having to work. So I sneaked the last issue into the wiki when probably no one was looking. 

On IRC the question was raised, whether nouveau would be working on NetBSD to which the answer was "no". Some netbsd user came forward and pointed us to [[http://mail-index.netbsd.org/tech-x11/2007/04/25/0000.html|http://mail-index.netbsd.org/tech-x11/2007/04/25/0000.html]] which is something we totally missed. So if any of you NetBSD developers are reading our TiNDC: We are _very_ interested in *BSD patches and will gladly apply those patches to our code base. So if anything more recent exists: Please do come forward :) 

JussiP fixed the dump status page, so you'll finally see correctly aging dumps :) 


### The Current status

On the account that PPC wasn't working: Marcheu found bugs in the DRM related to PPC and fixed them ( [[http://gitweb.freedesktop.org/?p=mesa/drm.git;a=commit;h=cd19dcef4f7cc454f68618a0a1e903f159db21ad|http://gitweb.freedesktop.org/?p=mesa/drm.git;a=commit;h=cd19dcef4f7cc454f68618a0a1e903f159db21ad]] ). Apart from bad programming on our side, we did have the additional problem that accessing the BIOS after start up didn't work correctly as the BIOS was showing signs of corruptions (which couldn't be, as it obviously worked when starting up). malc0 was bitten by this last year and we fixed the issue by copying the BIOS to RAM and using that image to work with (for DCB parsing etc).  [[http://gitweb.freedesktop.org/?p=mesa/drm.git;a=commit;h=de522ae742bd058780135eb21fe287e9a9dc263a|http://gitweb.freedesktop.org/?p=mesa/drm.git;a=commit;h=de522ae742bd058780135eb21fe287e9a9dc263a]] 

However, we still had failure reports, showing that we still hit this problem now and then. Marcheu now moved the copying to as early in the driver startup as possible , hopefully fixing this issue on PPC. 

AndrewR sent in a fix for overlay control which was cleaned up by ahuillet a bit ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-07.htm#1348|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-07.htm#1348]]).  It should give Xv brightness and colour intensity control and other goodies for  NV04/NV05 cards. 

Regarding Xv there were some fixes by marcheu to sync the blitter to the video output in order to avoid tearing on NV40. 

Stillunknwon finally bit the bullet and started to work on Randr1.2 for older cards (NV1x and NV2x too). Together with Ahuillet he worked out some problems. 

His work plus some more patches from malc0 did during the Holidays: 

* Dual link dvi may work, additional testers needed - 7300 go on  LVDS was tested and updated by stillunknown and 
* seventhguardian but first tries didn't yield useful results. [[!img http://people.freedesktop.org/~hughsient/temp/nouveau-split.jpg] 
* After a few days of prodding, hughsie came back an reported the same breakage (see pic) on a similar system, so LVDS was officially broken (hey, that much better than it's previous status of "unsupported"!) Seventhguardian with the help  of stillunknown finally got his issue solved by trial and error within nv_crtc.c. 
* 7300 go did get Randr1.2 modesetting fixes. 
Additionally, stillunknown tried his luck on textured video (via shaders)  for video playback on NV4x. The blitter ahuillet has done works fine, but  is a little bit on the slow side, so there is room for improvement if we could use shaders. 

Stillunknown got some basics explained by darktama and thunderbird and  after some prodding got a simple test program to work. A few days later  he got a gray scale yv12 adapter working. Further work slowed down though  as stillunknown hit some problems in understanding the shader code instructions correctly. He posted his current work for review which was done by Thunderbird and marcheu. 

Fixing up the shader code according to some of the feedback, a day later, he got colour working too. However, bi linear filtering caused problems  ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-12-29#1217|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-12-29#1217]]) 

Later, ahuillet did some improvement to the blitter image quality: linear interpolation in YV12->YUY2 conversion ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-04.htm#2134|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-04.htm#2134]]) 

And being in code mode he added another improvement to Xv: The overlay now  works slightly better with dual head setups. It changes the CRTC fine and  falls back on the blitter so that you never get to see a blue window instead  of your video. ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-06.htm#1547|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-06.htm#1547]]) 

Short topics: 

* Ahuillet gave up on PPC A8+A8 [[PictOp|PictOp]] and disabled it (well, at least he announced it :) ) 
* AndrewR and fsteinel_ reported TNT2 problems. AndrewR bisected it and found the bad commit. It was a problem with [[ImageFromCpu|ImageFromCpu]] which he later changed from NV05_IMAGE_FROM_CPU to NV_IMAGE_FROM_CPU which seemed to fix his problems. 
* Malc0 fixed some NV30 breakage for AGP cards by turning AGP in DRM off and on again ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-12-28.htm#1353|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-12-28.htm#1353]]) It was later cleaned up and committed by Stillunknown. 
* marcheu is still reworking the Xv code. It will later provide only one  generic video adapter which switches to the needed one (e.g. overlay, blitter, etc.) on the fly. 
* Marcheu did a sync to vblank patch for the xv texture adapter. Additionally he did more work on the texture adapter (filtering, optimization etc.), but this work is not finished yet. 
* ahuillet got a report from AndrewR (the multi card man :) ) that his NV05 wasn't working. Ahuillet didn't believe it at first but found evidence in the logs AndrewR provided. Strangely, the NV05 seems to use software methods for some features too. This fact was new to Ahuillet and set off to investigate the problem. A few days later darktama found the problem. Some commit resulted in clearing the bits that tell the card to handle software methods itself, resulting in the interrupt storm. After the bit got set again, everything was working. 
* jkolb added the context voodoo for NV86 cards. 
* Marcheu's work on the gallium framework on older cards has stalled somewhat due to real life constraints. 
(IRC: [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-11.htm#0248|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-11.htm#0248]]) 

On newer kernels (>= 2.6.24) the page fault notifiers are gone and thus MMioTrace stops working. PQ asked for help on the LKML ([[http://marc.info/?t=119982207100002&r=1&w=2|http://marc.info/?t=119982207100002&r=1&w=2]]) but got a negative response. So for now, if you do want to use MMioTrace don't use a kernel >=2.6.24. PQ is thinking about to fix that problem. Meanwhile Airlied, benh and others came forward to support PQ on LKML asking to revert the patch or at least offer something similar. It seems that PQ will prepare everything for inclusion into 2.6.25 and will get help from the kernel gurus in finding a replacement for the axed functions. 

And finally: We had several feedbacks that PPC was still working (Yes working!). If you are a PPC user, please do keep checking for regressions as we need to be notified ASAP about PPC regressions. 


### Help needed

NV4x users should test whether the textured video adapter works for them or not  and give feedback to stillunknown. 

Any NV04 / NV05 should test current git code and report back whether it works for them or not. 

As the randr1.2 code changes often, do test often. Do point out regressions to malc0 and stillunknown too, should you find some. 

And as always, please have a look at the "Testers wanted" page for requirements coming up between our issues. [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 

[[<<< Previous Issue|Nouveau_Companion_32]] | [[Next Issue >>>|Nouveau_Companion_34]] 
