[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[TiNDC 2008|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_38]]/[[ES|Nouveau_Companion_38-es]]/[[FR|Nouveau_Companion_38-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 6 de Abril


### Introducción

Han pasado apenas tres semanas desde el último número, así que ha llegado la hora de actualizarnos en cuanto al estado actual de las cosas. 

Bien, existe un patrón claro a la hora de intentar obtener un puesto en el Summer of Code que se repite cada año: ponernos en marcha solamente cuando apenas queda tiempo para el envío de las propuestas (a menos de 7 días). Afortunadamente el plazo se ha alargado hasta el 7 de abril, por lo que todavía tenéis una oportunidad de participar. Ponéos en contacto con nosotros si os interesa. 

Ha vuelto a suceder este año, corriendo atropelladamente para lograr que los estudiantes solicitasen un proyecto de Nouveau. Lo que tenemos es: 

* yeknom (Suspender / Continuar para NV4x) 
* riverbank (Motor sencillo de Gallium3D para chipsets NV2x) 
* Cierto soporte de XvMC para nouveau 
* Decodificación genérica de Vídeo por hardware. No está directamente relacionado con Nouveau pero nos beneficiaría igualmente. 
Stillunknown consiguió el dinero para comprar una tarjeta 8400 Gt por una donación de mycroes, por lo que ahora ya puede trabajar también en el establecimiento de modo de las NV5x. No estoy seguro de si lo agradece o se arrepiente de ello :). 

Sin embargo, mycroes, muchas gracias por esta generosa donación. 

Con todo, las semanas entorno a la semana santa fueron poco productivas. Un montón de temas personales y las propias vacaciones exigían más interacción social. 

Así que, sin más dilación, sigamos con los contenidos reales. 


### Estado actual

Ahora que stillunknown tiene su propia tarjeta NV5x ha empezado a reunir información sobre el estado actual de las NV5x. Comenzó limpiando y aclarando parte del código de establecimiento de modo que heredamos de nv. La conexión en caliente de monitores DVI es el siguiente tema de su agenda, y para ello (y otros retos relacionados con el establecimiento de modo) está investigando las interrupciones que emite la tarjeta. 

pq cambió nuestro estado actual, mientras que ahuillet y stillunknown actualizaron la tabla de estado. El texturado e iluminación se puso a N/A (no disponible), ya que, al menos de las NV40 en adelante, esto se hace a través de funciones de sombreado. Lo mismo debería aplicarse a las NV30. 

pq está trabajando todavía en los parches para tener listo MMioTrace e integrarlo en el kernel oficial. Todavía hay algunos problemas, tal vez incluso en el código subyacente de ftrace, pero se está avanzando. El caso de un solo procesador funciona bien con los últimos parches para el kernel 2.6.25rc que se pueden encontrar aquí: [[http://www.nvnews.net/vbulletin/showthread.php?t=110088|http://www.nvnews.net/vbulletin/showthread.php?t=110088]] SMP puede dar errores como "recursive probe hit" o bloqueos. 

Respecto a los bloqueos: parece que el driver binario, en la versión 100.169.x y siguientes se puede trazar de nuevo con MMioTrace (el usado en kernel más antiguos). Por lo que si quieres hacer un trazado de tarjetas NV5x, por favor usa un driver binario reciente. 

Malc0 ha estado experimentando con el establecimiento de modo en el kernel. Ha implementado un controlador de framebuffer para Nouveau, nouveaufb, muy básico, para NV04-NV4x que proporciona una consola de framebuffer y permite que las X inicien usando el controlador FBDEV. Es establecimiento de modo se hace en el kernel, pero en estos momentos no proporciona ningún tipo de aceleración. 

Malc0: "También merece la pena [...] señalar que se trata de código fantásticamente horrible :-D Hice lo mínimo posible para hacerlo funcionar". Este controlador es simplemente una prueba conceptual y se rehará adecuadamente a partir de ahora. 

Ahora pasamos a Marcheu, que acaba de terminar el ensamblado de su nueva máquina con la NV5x que recibió como donación (ver el número anterior). Antes de pasarse a trabajar con NV5x querría ver terminadas algunas cosas: 

* Antes de pasar el código a p0g quiere tener una arquitectura correcta para NV1z. Ahora glxinfo ya no falla y lo siguiente es tener los átomos de estado (objetos) bien. Después de eso, p0g debería ser más que capaz de dejarlo todo en buna forma. 
* Es necesario arreglar también algunas cosas para las NV3x para una arquitectura correcta. 
* Finalmente, es el momento de pasarse a codificar para NV5x. 
Ahora que nos hemos ocupado de Marcheu, stillunknown, pq y Malc0, veamos qué está haciendo Darktama. 

Bien. Escribió un programa de prueba que se comunica directamente con el hardware de las NV5x. El programa es capaz de mostrar triángulos ignorando por el momento el inexistente soporte de DRM que tenemos. 

Eso puede parecer poca cosa, pero es un inicio. La prueba de que podemos hacer que el motor 3D no solamente limpie algo (cosa que se consiguió en el último número), sino mostrar algo. Esto no fue fácil. Démosle una palmadita en la espalda :). 

Para que no se le acuse de vagancia, Darktama continuó su trabajo en las NV4x. No va a llevarle mucho más, porque ya casi está rematado el 3D en NV4x. En estos momentos quedan pendientes únicamente dos temas importantes: 

La gestión de memoria todavía no usa TTM. Y esto significa que los clientes OpenGL, que funcionan, acaban fallando al rato de iniciarse. El tiempo que tarden en hacerlo depende del programa y el hardware utilizados, pero podéis estar seguros, acaba fallando. 

El otro problema es que cada vez que motramos algo, *tenemos* que enviar las instrucciones a la GPU, y eso es malo por las siguientes razones: 

* el rendimiento de las GPUs es mejor cuando pueden procesar un bloque grande de instrucciones 
* con el TTM, se harán demasiadas llamadas al kernel para nuestro gusto 
En estos momentos, al mostrar una escena, el controlador hará: una limpieza (hasta tres veces), envío de instrucciones, mostrar algo, envío de instrucciones, intercambio de buffer, envío de instrucciones, en los que cada envío de instrucciones supone el acceso al kernel al usar el TTM. Esa no es, obviamente, la mejor solución, por lo que es necesario darle una vuelta. 

Lo de la limpieza hasta tres veces merece una explicación: Tiene que ver con cómo maneja Gallium el limpiado de pantalla. Lo hace saltándose el driver y lo hace usando cuadriláteros para cada buffer. El hardware de NVidia es capaz de usar métodos mejores, por lo que esa solución es mala. 

Respecto al TTM: aparte del problema antes indicado, Nouveau ya casi está listo para trabajar con él. Fundamentalmente, sólo queda que alguien "simplemente" (*ejem*, es necesario algún conocimiento del controlador) conecte las llamadas desde Nouveau al TTM. 

Marcheu está seguro de que se puede hacer de otra manera, manteniendo los objetos fijados (pinned) por el propio TTM. Pero esto necesita más investigación técnica. 

Antes de que salgáis corriendo de alegría: todavía faltan algunas cosas por hacer. Todavía tenemos problemas en nuestra parte (que mencionamos en este número) así como problemas en la parte de Gallium. 

Por ejemplo, al intentar hacer funcionar los reflejos, nos dimos cuenta de que no es sencillo hacerlo con el Gallium actual. Resumiendo: en Nvidia es en estos momentos imposible lograr la funcionalidad de glClipPlane() sin recurrir a funciones de sombreado. Sin embargo, esa es exactamente una cuestión de Gallium. La única solución por ahora es usar un respaldo por software, que hace las cosas un poco más lentas (probad Neverball con y sin reflejos si os interesa comprobarlo). Darktama tiene un parche listo para Gallium que está siendo revisado en estos momentos por los gurús de Gallium. Echad un vistazo aquí para una explicación más detallada: 

[[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2008-03-29#1425|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2008-03-29#1425]] 

Otros temas que merecen una mención: 

* Malc0 arregló el código de retroiluminación de los portátiles Apple. 
* Se escribieron algunas pruebas para el establecimiento de modo e ingeniería inversa de la visualización 3D en NV5x 