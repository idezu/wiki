# Memory Mapped I/O Trace

Mmiotrace is a toolbox for tracing memory mapped I/O access within the kernel. It can be used to record how a driver module uses MMIO reads and writes, for reverse engineering and debugging. For reverse engineering, you will need to have the proprietary driver, but it can also be useful for debugging the nouveau driver itself. 

The supported architectures are x86 and x86_64, they both seem to be pretty stable. If you need help, you can try to ask on _#nouveau_ IRC channel on OFTC.

Developers and analysts might be interested in [[MmioTraceDeveloper]] and old debugging notes in [[MmioTraceDebugging]]. The user space tools (for analysts) are mentioned in [[MmioTraceDeveloper]]. 

Features: 

* timestamped events 
* a mechanism to inject markers into the log while tracing 
* no kernel patching 

The modern format of an mmiotrace log is described in [[MmioTraceLogFormat]]. 

The latest kernel (or at least 2.6.29) is the recommended one, if you can choose.  

Mmiotrace is in the kernel, no need to fetch or patch anything extra. Please read the instructions in [[Documentation/trace/mmiotrace.txt|http://git.kernel.org/?p=linux/kernel/git/torvalds/linux-2.6.git;a=blob;f=Documentation/trace/mmiotrace.txt]]. No special programs are needed to record the output.


## Tracing the NVIDIA proprietary driver

If you want to trace binary Nvidia drivers you might want to read [[Ubuntu X/MMIOTracing guide|https://wiki.ubuntu.com/X/MMIOTracing]]. Some basic steps are also available below:

* Start tracing
* Load nvidia.ko and start X up with the nvidia proprietary driver
* Run some 3d for a few seconds (e.g. fire up glxgears)
* Stop tracing and send the generated output to mmio.dumps at gmail.com


## Submitting results

If you want to send mmiotrace dumps to us, please send them to _mmio dot dumps at gmail dot com_. Please, pack into a compressed archive (xz) the trace file and a free-form description about what you do during the trace. The output of `lspci` is already included in the trace file, but do add the output of `uname -a` and the driver version into your description. Tell also what display connectors you have, what kind of monitors you have connected to what outputs, whether it has tv-in/out, and what display mode you used. The name of the archive file should contain the PCI id and GPU family, or the commercial name of your card. Thank you. 

## Analyzing results

There is a `demmio` tool in [[envytools|http://github.com/envytools/envytools]] which annotates known registers based on its database of registers. It also adjusts addresses based on the PCI mappings, making it much easier to read/understand the trace.
