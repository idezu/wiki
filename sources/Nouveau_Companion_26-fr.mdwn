[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_26]]/[[ES|Nouveau_Companion_26-es]]/[[FR|Nouveau_Companion_26-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 22 août 2007


### Introduction

Voici la première version du TiNDC qui sera publié en premier lieu sur Phoronix.com. Nous avons décidé cela pour plusieurs raisons : 

* Une meilleure couverture médiatique qui attirera peut-être plus de contributeurs 
* Mieux communiquer sur l'avancement du projet, il semble que le TiNDC n'est pas lu aussi souvent que nous le voudrions 
* Une meilleure couverture signifie un public mieux informé, ce qui, espérons le, se traduira par un peu plus de contributions et quelles seront de bonnes qualités 
Qu'est ce qui change alors ? A part le site sur lequel nous publions en premier, rien du tout. Phoronix.com publiera la version anglaise en premier et 2 à 3 jours plus tard, elle sera publiée sur le wiki accompagnée des traductions. 

Merci à Phoronix.com de nous permettre de toucher un large public. 

Petite explication pour ceux d'entre vous qui liraient le TiNDC pour la première fois : Le compagnon irrégulier du développement de Nouveau (The irregular Nouveau Development companion) est un résumé libre du développement du projet Nouveau. Cela inclut principalement les discussions sur IRC (#nouveau sur freenode), sur la mailing list, et, si nécessaire, les commits dans l'arbre Git.  Si vous préférez les versions traduites (français et espagnol), jetez un oeil au wiki, elles seront en ligne d'ici quelques jours. 


### Statut actuel

La majorité du travail se fait actuellement sur la partie 2D, en grande partie pour faire fonctionner Xv. Ahuillet et p0g ont réalisé des traces des registres MMIO pour YV12 et YUY2 et les ont comparées entre elles. Les résultats leur ont permis de déterminer quels registres étaient utilisés pour l'overlay (_NDT : écriture directe de l'image dans la mémoire vidéo, le travail de redimensionnement et autres étant fait par la carte et non par le CPU_). Le 10 août 2007, Ils annoncèrent avoir réussi et commencèrent à chercher des testeurs.  

À peu près au même moment, dagb a créé un [[tableau des fonctions|FeatureMatrix]] qui marchent (ou non) et celles sur lesquels nous travaillons (ou non). 

Finalement, nous avons plusieurs types d'overlays : 

* celui des NV04/NV05 fait seulement du YUY2 
* celui des NV10 et plus fait YUY2 
* celui des NV17 et plus fait également du YV12 
L'overlay fonctionne des NV1x jusqu'au NV3x et donne des performances plutôt correctes. si la video utilise le format YV12, il est converti à la volée au format NV12, qui est ce que les cartes Nvidia attendent. Évidemment, cela signifie que l'overlay NV12 fonctionne également. 

La première version avait un bug dans l'envoi de la surface de couleurs qui résultait en des couleurs manquantes si la vidéo était déplacée partiellement hors de l'écran. De même des corruption de l'image apparurent lors du test de l'overlay NV12/YV12, dues aux limites de la bande passante si la vidéo était de grande dimension (1024 pixels de large au moins). Ahuillet et p0g supposèrent que l'utilisation de deux surfaces par le format posait problème. Le pilote nv de Xorg, quand à lui, fait des choses bizarres avec les registres CRTC que nous ne comprenons pas bien. Ahuillet décida finalement de simplement retailler l'image pour ne traiter que la partie réellement affichée sur l'écran, avec le même résultat. Après cette correction, ahuillet et p0g se penchèrent sur le format YUY2. 

L'overlay des NV04 ne fonctionne pas pour le moment car ahuillet ne possède pas ce type de carte. Nous avons reçu quelques offres, autrement nous utiliserons eBay... Bref, dans tous les cas, le support de l'overlay sur NV04 devrait arriver dans un futur pas trop lointain. 

Mjules a testé le tout sur NV3x et il fonctionne, pq a testé sur NV28 sans problème également. Mais dcha notifia des corruptions d'images sur NV11 (!Geforce 2 Go) qui furent investiguées par ahuillet et p0g. L'explication est que les cartes < NV17 n'ont pas d'overlay YV12 et gèrent seulement le YUY2. L'overlay YV12 fut donc désactivé pour ces cartes. Quand au cartes >NV4x, elles n'ont pas d'overlay du tout et utilisent le blitter (_NDT : circuit sur la carte dédié à la combinaison de différents masque l'un au dessus de l'autre_) pour cette fonction. Ahuillet accéléra cette fonction également. 

ALors qu'ils testaient le pilote, matc et p0g obtinrent tous les 2 l'erreur INVALID_STATE émise par la carte. Ahuillet et p0g essayèrent d'isoler le bug et découvrirent rapidement qu'il était dans le code EXA. Et il devait être là depuis l'origine des temps (enfin, au moins depuis le début de ce projet) puisqu'il existait déjà dans la version 0.0.4 du DRM, obligeant à une autre approche pour le régler. Après quelques test, ahuillet et p0g remarquèrent que le problème apparaissait quand on essayait la fonction blend. Chaque fonction d'EXA utilisant ce type de traitement faisait apparaitre l'erreur. Xv était seulement une victime et crashait simplement parce qu'une erreur dans EXA se produsait juste avant. 

Avec le Vacation of Code arrivant à son terme, nous aimerions remercier Ahuillet pour son travail qui a permis d'obtenir une implémentation efficace et fonctionnelle de Xv pour les cartes NV18 à NV4x. Nous sommes heureux que tu aies choisi Nouveau (et encore plus que tu restes avec nous). 

pmdata est finalement arrivé à faire en sorte que REnouveau crée des dumps contenant uniquement les changements détectés, sans les interpréter, ce qui rend les fichiers beaucoup plus petits. Pour créer un dump texte interprété, il suffit d'utiliser renouveau-parse sur le fichier brut. Les données du parseur sont maintenant stockées dans une base XML. La prochaine étape sera de modifier le générateur du fichier nouveau_drm.h afin qu'il utilise aussi cette base de données. Pour le moment, en effet, il repose sur tableau nv_objects[] de REnouveau, lequel contient les même données que la base (en fait, toutes les données n'ont pas encore été migrées, mais ça ne saurait tarder). kmeyer a ainsi adapté son script d'upload de façon à prendre en compte le changement. Dans le même temps, JussiP annonça que son compte serait bientôt désactivé et qu'il ne pourrait plus mettre à jour la page de statut des dumps à partir du premier septembre 2007. Nous sommes en train de chercher d'autres solutions. 

cmn s'est penché sur les NV35 et glxgears. Le dernier statut remontait au Fosdem 2007, avec quelque chose qui fonctionnait mais des problèmes de matrices de projection. Actuellement, il corrige petit à petit les problèmes de TCL (Transform, Clipping & Lighting). Il reste néanmoins beaucoup à faire. Si la hauteur de la fenêtre est supérieure à la largeur, le résultat est une roue géante immobile, sans moyen de voir le reste. Si la largeur est supérieure à la hauteur, des couleurs clignotent (bleu surtout) et quelque chose de vaguement ressemblant avec une roue rouge apparait et disparait comme si elle se déplaçait autour d'un point. Donc même si le code s'est bien amélioré, le résultat est encore loin d'être parfait (comparé à ce à quoi on s'attend). Mais voyez par vous même : 

[[[[!img http://www.cmartin.tk/nouveau/gear1.png]|http://www.cmartin.tk/nouveau/gear1.png]] [[[[!img http://www.cmartin.tk/nouveau/gear2.png]|http://www.cmartin.tk/nouveau/gear2.png]]  

Il y a tout de même un problème avec les NV3x : l'initialisation 3D de la carte n'est pas faite correctement. Et donc, démarrer X avec Nouveau ne vous mènera pas très loin. Il faudra charger le blob en premier, il initialisera correctement la carte, ce que nous ne faisons pas encore, puis décharger le blob avant de lancer X avec nouveau, qui fonctionnera. Cela signifie une chose ; notre code pour basculer entre les contextes est correct mais le code d'initialisation manque des quelques morceaux importants. Marcheu travaille dessus et cherche ce que ces morceaux peuvent bien être. Malheureusement, pour trouver les bonnes valeurs, il doit éplucher 90 Mo de dump MMIO. ça prend du temps (mais il est presque à la fin, il ne lui reste que 250 ko). 

Quelques brèves : 

* le générateur de nouveau_drm. était cassé pour le mode par défaut (il créait des #defines inutilisables). corrigé par KoalaBR 
* Createdump.sh s'est vu ajouté des tests pour les entêtes SDL et la bibliothèque libXvMVNVIDIA.so pour que renouveau puisse compiler 
* pmdata a implémenté des commandes clear fonctionnelles pour les cartes NV04 à NV1x. Maintenant, les parties de votre écran qui doivent réellement être nettoyées le sont. On manque encore de tests et de retours à ce sujet néanmoins. 
* Un disque dur de Marcheu a crashé et il a perdu différents patch pour faire fonctionner les NV04 sur le DRM actuel, ils corrigeaient la bascuel entres contextes. Il pourra les réécrire (et le fera) mais c'est toujours une perte de temps ennuyeuse. 

### Aide requise

Ok les gars, (et les filles), soyons sérieux. Vous voulez un pilote libre pour les cartes Nvidia ? Fort bien, nous avons besoin de votre aide et cette section vous dit comment faire pour sauver un chaton. Choisissez ce que vous voulez et s'il vous plait, contactez nous. 

Vous pouvez tester Xv avec notre pilote. Il suffit simplement de regarder une vidéo avec votre lecteur préféré (mplayer, codeine, etc) et de préciser à ahuillet si cela fonctionne pour vous ou non. 

Nous apprécierions que des propriétaires de 8800 testent le pilote actuel et nous fassent part du résultat. Nous n'avons actuellement que deux cartes G84 pour développer et tester, et le retour d'autres utilisateurs de ces cartes serait bienvenue. Note : utilisez la branche randr-1.2 et faire remonter les résultats à Darktama. 

Et nous aurions besoin de dump [[MmioTrace|MmioTrace-fr]] pour les cartes NV41, NV42, NV44,NV45, NV47,NV48 et NV4C. Pour les possesseurs de NV3x, nous aurions également besoins de dumps MMIO des cartes des types suivants : 10de:030x / 0x10de:0x00fc / 0x10de:0x00fd / 0x10de:0x00fe. Faites vous connaitre auprès de Darktama (NV4x) ou Marcheu (NV3x) sur le canal si vous pouvez nous aider. 

Voilà, c'en est fini de l'édition #26 qui marque mon premier anniversaire (_NDT : de KoalaBR_) en tant que participant de ce projet. Bien qu'irrégulier, nous avons essayé de publier une édition toutes les 2 semaines environs cette année. La prochaine édition devrait être un peu en retard, étant donné que je serais en vacance jusqu'à la fin du mois de septembre. Nous tenterons tout de même de répondre aux questions postées sur le forum de Phoronix.com dans les deux prochaines semaines. 

[[<<< Édition précédente|Nouveau_Companion_25-fr]] | [[Édition suivante >>>|Nouveau_Companion_27-fr]] 
