The easy way to recover from a lockup is a reboot. You probably want to use sysrq to reboot cleanly, for instance : sysrq + REISUB. See [[http://en.wikipedia.org/wiki/Magic_SysRq_key|http://en.wikipedia.org/wiki/Magic_SysRq_key]] 

The alternative is to suspend, by following these steps : 

* release the keyboard : sysrq + r 
* kill X (it should be already dead) : sysrq + k 
* switch to a VT and login : ctrl+alt+f1 (this step and the next ones might have to be done blindly) 
* unload the nouveau kernel module : [[KernelModeSetting#unload]]
* suspend to RAM : echo mem > /sys/power/state / pm-suspend / etc 
* wake up 
* reload nouveau 
